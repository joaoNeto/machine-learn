import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

beer_dataframe = pd.read_csv('dados/beer_dataset2.csv', sep=';')
linhas, colunas = beer_dataframe.shape

# TRATANDO OS DADOS
# beer_dataframe['Temperatura Media (C)'] = beer_dataframe['Temperatura Media (C)'].str.replace(',', '.')
# beer_dataframe['Temperatura Minima (C)'] = beer_dataframe['Temperatura Minima (C)'].str.replace(',', '.')
# beer_dataframe['Temperatura Maxima (C)'] = beer_dataframe['Temperatura Maxima (C)'].str.replace(',', '.')
# beer_dataframe['Precipitacao (mm)'] = beer_dataframe['Precipitacao (mm)'].str.replace(',', '.')

# GERANDO GRÁFICO CONSUMO POR DIA
fig, ax = plt.subplots(figsize=(20, 6))
ax.set_title('Consumo de Cerveja', fontsize=20)
ax.set_ylabel('Litros', fontsize=16)
ax.set_xlabel('Dias', fontsize=16)
ax = beer_dataframe['Consumo de cerveja (litros)'].plot()
plt.show()
plt.savefig('graph_img/consumo_cerveja_dia.png')
plt.clf()

# GRAFICO OUTLIERS BOXPLOT
sns.boxplot(
  data=beer_dataframe,
  x='Final de Semana',
  y='Consumo de cerveja (litros)'
  # orient='h'
)
plt.show()
plt.savefig('graph_img/fora_da_curva_final_semana.png')
plt.clf()

# GRAFICO OUTLIERS BOXPLOT
sns.boxplot(
  data=beer_dataframe['Consumo de cerveja (litros)'],
  orient='h'
)
plt.show()
plt.savefig('graph_img/fora_da_curva_geral.png')
plt.clf()


# RELACAO ENTRE AS VARIAVEIS
sns_plot = sns.pairplot(
  beer_dataframe,
  y_vars='Consumo de cerveja (litros)', 
  x_vars=['Temperatura Media (C)', 'Temperatura Minima (C)', 'Temperatura Maxima (C)', 'Precipitacao (mm)', 'Final de Semana'],
  size=5,
  kind='reg'
)
sns_plot.savefig("graph_img/relacionamento_entre_variaveis.png")

"""
analisando a relação de variáveis é que faz sentido chamar de regressão linear. Quanto maior o valor de uma variavel X, maior também será a de Y e quando menor 
a de X, menor será a de Y, ou seja, as duas variáveis possuem alguma relação.
"""

print('finish')