import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics

beer_dataframe = pd.read_csv('data.csv', sep=',')
linhas, colunas = beer_dataframe.shape

# TRATANDO OS DADOS
beer_dataframe['Temperatura Media (C)'] = beer_dataframe['Temperatura Media (C)'].str.replace(',', '.')
beer_dataframe['Temperatura Minima (C)'] = beer_dataframe['Temperatura Minima (C)'].str.replace(',', '.')
beer_dataframe['Temperatura Maxima (C)'] = beer_dataframe['Temperatura Maxima (C)'].str.replace(',', '.')
beer_dataframe['Precipitacao (mm)'] = beer_dataframe['Precipitacao (mm)'].str.replace(',', '.')

# APLICANDO MACHINE LEARNING
var_alvo = beer_dataframe['Consumo de cerveja (litros)']
var_preditora = beer_dataframe[['Temperatura Maxima (C)', 'Final de Semana', 'Precipitacao (mm)']]

preditora_treino, preditora_teste, alvo_treino, alvo_teste = train_test_split(var_preditora, var_alvo, test_size = 0.3, random_state=2811)

modelo = LinearRegression()
modelo.fit(preditora_treino, alvo_treino)

# o coeficiente de determinação é uma medida resumida que diz quanto a linha de regressão se ajusta aos dados, é um valor entre 0 e 1
coeficiente_determinacao = modelo.score(preditora_treino, alvo_treino)

# gerando previsoes para os dados de teste
alvo_previsto = modelo.predict(preditora_teste)

# % de previsao
metrics.r2_score(alvo_teste, alvo_previsto)

# PREVENDO UM CASO
meu_teste = [
  [32.5, 0, 0],
  [29.0, 0, 0],
  [35.8, 1, 0]
]

print(modelo.predict(meu_teste))

"""
valor correto, valor da predicao


25.461, 27.95 
22.446, 25.70 
37.690, 35.42

"""