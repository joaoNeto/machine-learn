import pandas as pd 
from sklearn.naive_bayes import MultinomialNB, GaussianNB, BernoulliNB
from sklearn.model_selection import train_test_split

df = pd.read_csv('data.csv')

var_preditora = pd.get_dummies(df[['home', 'busca', 'logado']])
var_alvo = df['comprou']

preditora_treino, preditora_teste, alvo_treino, alvo_teste = train_test_split(var_preditora, var_alvo, test_size = 0.3)

teste = [
    [0,1,1,0,0], # 1
    [1,0,1,0,0]  # 0 
]


def ml_multinomial():
    model = MultinomialNB()
    model.fit(preditora_treino, alvo_treino)
    acuracia = model.score(preditora_teste, alvo_teste)
    resultado = model.predict(teste)
    return (acuracia, resultado)


def ml_bernoulli():      
    model = BernoulliNB()
    model.fit(preditora_treino, alvo_treino)
    acuracia = model.score(preditora_teste, alvo_teste)
    resultado = model.predict(teste)
    return (acuracia, resultado)


def ml_gaussian():
    model = GaussianNB()
    model.fit(preditora_treino, alvo_treino)
    acuracia = model.score(preditora_teste, alvo_teste)
    resultado = model.predict(teste)
    return (acuracia, resultado)


acuracia_multinominal, resultado_multinomial = ml_multinomial()
acuracia_bernoulli, resultado_bernoulli = ml_bernoulli()
acuracia_gaussian, resultado_gaussian = ml_gaussian()


print(f'acuracia multinominal.: {acuracia_multinominal}')
print(resultado_multinomial)

print(f'acuracia bernoulli.: {acuracia_bernoulli}')
print(resultado_bernoulli)

print(f'acuracia gaussian.: {acuracia_gaussian}')
print(resultado_gaussian)