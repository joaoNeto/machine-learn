# Naive Bayes

O algoritmo [naive bayes](https://en.wikipedia.org/wiki/Naive_Bayes_classifier) é um classificador probabilístico baseado no “Teorema de Bayes”. Por ser muito simples e rápido, possui um desempenho relativamente maior do que outros classificadores. Além disso, o Naive Bayes só precisa de um pequeno número de dados de teste para concluir classificações com uma boa precisão.
A principal característica do algoritmo, e também o motivo de receber “naive” (ingênuo) no nome, é que ele desconsidera completamente a correlação entre as variáveis (features). Ou seja, se determinada fruta é considerada uma “Maçã” se ela for “Vermelha”, “Redonda” e possui “aproximadamente 10cm de diâmetro”, o algoritmo não vai levar em consideração a correlação entre esses fatores, tratando cada um de forma independente. O Sklearn implementa o Naive Bayes de 3 formas:

  - [Gaussian](https://scikit-learn.org/stable/modules/generated/sklearn.naive_bayes.GaussianNB.html#sklearn.naive_bayes.GaussianNB)
  - [Multinomial](https://scikit-learn.org/stable/modules/generated/sklearn.naive_bayes.MultinomialNB.html#sklearn.naive_bayes.MultinomialNB)
  - [Bernoulli](https://scikit-learn.org/stable/modules/generated/sklearn.naive_bayes.BernoulliNB.html#sklearn.naive_bayes.BernoulliNB)
 
### Quais são os prós e contras de Naive Bayes?

#### Prós:

- É fácil e rápido para prever o conjunto de dados da classe de teste. Também tem um bom desempenho na previsão de classes múltiplas.
- Quando a suposição de independência prevalece, um classificador Naive Bayes tem melhor desempenho em comparação com outros modelos como regressão logística, e você precisa de menos dados de treinamento.
- O desempenho é bom em caso de variáveis categóricas de entrada comparada com a variáveis numéricas. Para variáveis numéricas, assume-se a distribuição normal (curva de sino, que é uma suposição forte).

#### Contras:

- Se a variável categórica tem uma categoria (no conjunto de dados de teste) que não foi observada no conjunto de dados de treinamento, então o modelo irá atribuir uma probabilidade de 0 (zero) e não será capaz de fazer uma previsão. Isso é muitas vezes conhecido como “Zero Frequency”. Para resolver isso, podemos usar a técnica de alisamento. Uma das técnicas mais simples de alisamento é a chamada estimativa de Laplace.
- Por outro lado naive Bayes é também conhecido como um mau estimador, por isso, as probabilidades calculadas não devem ser levadas muito a sério.
- Outra limitação do Naive Bayes é a suposição de preditores independentes. Na vida real, é quase impossível que ter um conjunto de indicadores que sejam completamente independentes.

O algoritmo Naive Bayes é facilmente aplicado em cenários como: Previsões em tempo real, Previsões multi-classes, Classificação de textos/Filtragem de spam/Análise de sentimento e Sistema de Recomendação.

### Gaussian
```sh
from sklearn.naive_bayes import GaussianNB
```
Deve ser usada para feições em forma decimal. O GNB assume que os recursos seguem uma distribuição normal.

### Multinomial
```sh
from sklearn.naive_bayes import MultinomiaLNB
```
Deve ser usado para os recursos com valores discretos como contagem de palavras 1,2,3 ...

### Bernoulli
```sh
from sklearn.naive_bayes import BernoulliNB
```
Deve ser usado para recursos com valores binários ou booleanos como True / False ou 0/1.


### Links úteis

- https://www.vooo.pro/insights/6-passos-faceis-para-aprender-o-algoritmo-naive-bayes-com-o-codigo-em-python/