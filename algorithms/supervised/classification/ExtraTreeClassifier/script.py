import pandas as pd 
from sklearn.model_selection import train_test_split
from sklearn.ensemble import ExtraTreesClassifier # algoritmo de machine learning

arquivo = pd.read_csv('data.csv')

arquivo['style'] = arquivo['style'].replace('red', 0)
arquivo['style'] = arquivo['style'].replace('white', 1)

var_alvo = arquivo['style'] # variável que nós queremos prever.
var_preditora = arquivo.drop('style', axis = 1) # variáveis que são ponderadas para se chegar no valor da variável alvo.

preditora_treino, preditora_teste, alvo_treino, alvo_teste = train_test_split(var_preditora, var_alvo, test_size = 0.3) # 30% dos dados vão servir pra teste e o restante ele irá treinar, isso foi definido na variável test_size

modelo = ExtraTreesClassifier()
modelo.fit(preditora_treino, alvo_treino) # aplica o modelo de machine learning, é no método fit que ele vai treinar os dados

acuracia = modelo.score(preditora_teste, alvo_teste) # é no método score que o algoritmo vai testar se está prevendo os valores da variável alvo como deveria


'''
6,0.21,0.38,0.8,0.02,22,98,0.98941,3.26,0.32,11.8,6,white
7.8,0.88,0,2.6,0.098,25,67,0.9968,3.2,0.68,9.8,5,red
'''

listaTipoVinho = modelo.predict([[6,0.21,0.38,0.8,0.02,22,98,0.98941,3.26,0.32,11.8,6]])

print('Minha acuracia é de.: ', acuracia)

if(listaTipoVinho[0]):
  print('O vinho é branco')
else:
  print('O vinho é tinto')
