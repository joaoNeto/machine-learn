import sys
import nltk

class NaturalLanguage():


    def __init__(self):
        self.stopwords = nltk.corpus.stopwords.words("english")
        self.stemmer = nltk.stem.snowball.EnglishStemmer()
        self.dictionary_ml = []


    def tokenize(self, my_list):
        return [nltk.tokenize.word_tokenize(phrase) for phrase in my_list.str.lower()]


    def create_dictionary(self, my_list_phrases):        
        my_list_phrases = self.tokenize(my_list_phrases)
        dictionary = set()

        for phrase in my_list_phrases:
            try:
                word_validated = [self.stemmer.stem(word) for word in phrase if word not in self.stopwords and len(word) > 2]
                dictionary.update(word_validated)
            except:
                print(f"WARNING: creating dictionary '{phrase}'")

        tuplas = zip(dictionary, range(len(dictionary)))
        self.dictionary_ml = {palavra: indice for palavra, indice in tuplas}


    def vectorize_text(self, phrase):
        vector = [0] * len(self.dictionary_ml)
        for word in phrase:
            if len(word) > 0:
                root = self.stemmer.stem(word)
                if root in self.dictionary_ml:
                    position = self.dictionary_ml[root]
                    vector[position] += 1
        return vector


    def change_phrases_to_nl(self, my_list):
        my_list = self.tokenize(my_list)
        return [self.vectorize_text(phrases) for phrases in my_list]
