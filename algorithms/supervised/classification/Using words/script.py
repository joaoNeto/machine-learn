import pandas as pd 
import numpy as np
from natural_language import NaturalLanguage
from sklearn.multiclass import OneVsRestClassifier, OneVsOneClassifier
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import AdaBoostClassifier
from sklearn.model_selection import train_test_split, cross_val_score
import sys
import nltk

naturalLanguage = NaturalLanguage()

dataset = pd.read_csv("data.csv")
dataset = dataset[dataset['selected_text'].str.len() > 2]

sentiments = ['neutral', 'negative', 'positive']

dataset['sentiment'] = dataset['sentiment'].replace('neutral', sentiments.index('neutral'))
dataset['sentiment'] = dataset['sentiment'].replace('negative', sentiments.index('negative'))
dataset['sentiment'] = dataset['sentiment'].replace('positive', sentiments.index('positive'))

target  = dataset['sentiment']
list_phrases_predict = dataset['selected_text']

naturalLanguage.create_dictionary(list_phrases_predict)
predictors = naturalLanguage.change_phrases_to_nl(list_phrases_predict)

predictor_training, predictor_test, target_training, target_test = train_test_split(predictors, target, test_size = 0.3)

# model_ml = OneVsOneClassifier(LinearSVC(random_state=0))
model_ml = MultinomialNB()

# testing accuracy for each ml model
scores  = cross_val_score(model_ml, predictor_test, target_test, cv=5)
acuracy = np.mean(scores)
print(f"minha acuracia é de {acuracy}")

# ANALYSING THE RESULTS

model_ml.fit(predictor_training, target_training)
print("terminou de treinar")
# PREDICTING ANOTHER PHRASES

list_test = [ 
    'i hate you',
    'i love everybody',
]

my_test = naturalLanguage.change_phrases_to_nl(pd.Series(list_test))
result = model_ml.predict(my_test)
i=0
for num_sentiments in result:
    phrase = list_test[i]
    i += 1
    print(f"The phrase '{phrase}' is {sentiments[num_sentiments]}")