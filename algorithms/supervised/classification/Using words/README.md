
- Desenvolver um algortimo que transforme uma frase em um dicionário de palavras
- As variáveis alvo devem conter mais de um tipo de valor para utilizar o OneVsOne and OneVsRest
- Utilizar o k-flow para fazer o embaralhamento dos dados
- Fazer comparações de acuracia entre algoritmos de ML
- Desenvolver um serviço HTTP onde o cliente envia uma mensagem e o algoritmo guarda esses dados e faz a sua análise



pip install nltk

nltk.download("stopwords")
nltk.corpus.stopwords.words("portuguese")



nltk.download("rslp") 
stemmer = nltk.stem.RSLPStemmer()
stemmer.stem("amigos") # remove o sufixo das palavras de lingua portuguesa