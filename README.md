## MACHINE LEARNING

##### <b>TÉCNICAS DE MACHINE LEARNING</b>

<b>aprendizado supervisionado</b> deve-se ter uma base com diversas amostragens e a máquina cria um padrão a partir desses dados, ex.:voce mostra para a máquina uma infinidade de fotos de gatos e informa pra ela que todas as fotos são gatos, quando voce mostrar um outro gato que não estava na base ele consegue distinguir se é ou não um gato com base no aprendizado que ele adiquiriu. Dentro do espectro do aprendizado supervisionado temos dois tipos de algoritmos.:


* <b>regressão</b>
tenta prever o valor de um dado, Sistemas de regressão poderiam ser usados, por exemplo, para responder às perguntas: 'Quanto custa?' ou 'Quantos existem?'. ex.: prever o preço de um produto.
	
* <b>classificação</b>
tenta prever se aqueles dados pertencem a uma categoria ou outra, ex.: a partir das caracteristicas prever se o vinho é branco ou tinto.

> OBS.: quando usar classificação ou regressão? algoritmos de classificação é quando voce precisa de uma resposta categorica, uma resposta simples de sim ou não. algoritmo de regressão queremos predizer o valor numérico, ex.: quantidade de meses que um cliente vai utilizar um serviço.

<b>aprendizado não supervisionado </b> esses algoritmos visam obter uma melhor representação dos dados, como em uma segmentação/classificação de clientes, e não usam rótulos pré-definidos. Dentro desse espectro do aprendizado não supervisionados teremos o seguintes algoritmos.:

* <b>clusterização</b> 
tentar achar determinados padrões, achar os grupos a partir de suas semelhanças

##### <b>DADOS DE TREINO E TESTES</b>

Os <b>dados de treino</b> são os dados que ele pode treinar, ex.: entregar um monte de dados com imagens de gatos. já os <b>dados de testes</b> são dados que não estão na base dos dados de treino, que são mostrados para validar se o algoritmo está reconhecendo padrões como deveria

##### <b>PROBLEMAS COM MACHINE LEARNING</b>

* <b>overfitting</b> esse é um problema que ocorre toda vez que voce treina muito seu algoritmo apenas com aqueles dados de treino, quando vai para os dados de testes ele não consegue prever por que o algoritmo já está viciado para se ajustar muito bem aos dados de treino.
* <b>underfitting</b> problema quando não consegue prever nem para os dados de treino nem os dados de testes.


##### <b>ETAPAS DO MACHINE LEARNING</b>

1. conhecimento dos dados => qual o problema voce vai resolver
2. pré processamento e seleção de variáveis => adequar e formatar os dados, tratar dados faltantes, tratamento de dados fora da curva (para não prejudicar a análise geral), etc.
3. Escolha a aplicação do modelo de machine learning
4. Medição dos resultados
5. Aperfeiçoamento (após essa etapa voce pode voltar a etapa 3)
6. Resultado final (após tantas combinações e modelos quando ve que a acuracia ja está alta pode a etapa de aperfeiçoamento sua aplicação já está pronta)


> OBS.: normalização de dados é levar todos os dados para a mesma ordem de grandeza para então fazer comparações necessárias, ex.:<br><br>
>	peso, altura, definicao<br>
>	70, 1.50, obeso<br>
>	50, 1.90, magro<br>
>	60, 1.40, ?<br><br>
>   para saber a definição da terceira pessoa eu preciso tirar a média dos pesos e a média das alturas, com isso eu tiro a média de cada peso em relação a média dos dados, fazendo isso eu transformo todos os dados na mesma grandeza.<br>
 
##### <b>PYTHON PARA MACHINE LEARNING</b>

###### SCIKIT-LEARN (https://scikit-learn.org/stable/)

Bibliotéca de machine learning

###### PANDAS (https://pandas.pydata.org/)

pandas fornece ferramentas de análise de dados e estruturas de dados de alta performance e fáceis de usar.
Pandas é fundamental para Análise de Dados, como ele voce pode gerar relatórios gráficos e fazer um detalhamento
da situação dos seus dados.

Uma Series é como um array unidimensional, uma lista de valores
>	notas = pd.Series([2,7,5,10,6])

um DataFrame é uma estrutura bidimensional de dados, como uma planilha
>	df = pd.DataFrame({'Aluno' : ["Wilfred", "Abbie", "Harry", "Julia", "Carrie"],<br>
>                   'Faltas' : [3,4,2,1,4],<br>
>                   'Prova' : [2,7,5,10,6],<br>
>                   'Seminário': [8.5,7.5,9.0,7.5,8.0]})<br><br>
>    df.describe()


para ler dados existem os comandos read_sql, read_csv, read_xls, etc.

###### NUMPY (https://numpy.org/)

pacote para operações matemáticas, com ele voce pode criar arrays unidimensionais, bidimensionais, pode criar matrizes com dados preenchendo dados automaticamente, pode ver o menor, o maior e a média do valor da matriz